import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  isAuthen = false;
  currentUser = localStorage.getItem('currentToken');
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService,) { }


  createAuthorizationHeader(headers: HttpHeaders) {
    headers.append( 'Content-Type', 'application/json' )
    if(this.tokenChecker())
    {
      let bearer = this.tokenGetter();
      headers.append('Authorization', 'Bearer ' + this.tokenGetter());
    }
  }
  tokenGetter() {
    let res = localStorage.getItem('currentToken');
    if(res != null){
      let r = JSON.parse(res);
      return r.token;
    }
  }
  tokenChecker(){
    let token = "";
    let userId = "";
    let userName = "";
    if(this.currentUser != null){
      let res = JSON.parse(this.currentUser);
      if(res != null){
        token = res.token;
        userId = res.id;
        userName = res.username;
      }
    }
    if(token != null && token.length > 0){
      if(!this.jwtHelper.isTokenExpired(token))
        this.isAuthen = true;
    }
    return this.isAuthen;
  }
  get(url:any) {
    let headers = new HttpHeaders();
    this.createAuthorizationHeader(headers);
    return this.http.get(url, {
      headers: headers
    });
  }

  post(url:any, data:any) {
    let headers = new HttpHeaders();
    this.createAuthorizationHeader(headers);
    return this.http.post(url, data, {
      headers: headers
    });
  }
}
