import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { LoginComponent } from './pages/account/login/login.component';
import { PagesModule } from './pages/pages.module';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/layout/header/header.component';
import { MeunuLeftComponent } from './components/layout/meunu-left/meunu-left.component';
import { JwtModule } from "@auth0/angular-jwt";
import { environment } from '../environments/environment';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { PushNotificationsService } from './components/pushnotifications/push-notifications.service';
import { AsyncPipe } from '../../node_modules/@angular/common';
import { HttpClientService } from './helper/http-client.service';
import {NhaCungCapComponent} from './pages/nha-cung-cap/nha-cung-cap.component';



const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'nha-cung-cap/:id', component: NhaCungCapComponent },


]; // sets up routes constant where you define your routes

export function tokenGetter() {
  let res = localStorage.getItem('currentToken');
  if(res != null){
    let r = JSON.parse(res);
    return r.token;
  }
}


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MeunuLeftComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    PagesModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
      },
    }),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireMessagingModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  exports: [RouterModule],

  providers: [PushNotificationsService, AsyncPipe, HttpClientService],
  bootstrap: [AppComponent]
})
export class AppModule { }
