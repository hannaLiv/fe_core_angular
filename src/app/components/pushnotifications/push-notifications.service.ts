import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs'
import * as APIHelper from '../../helper/APIDomain';
import { HttpClientService } from '../../helper/http-client.service';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationsService {

  currentMessage = new BehaviorSubject(null);
  base_domain_url = APIHelper.APIUrl + 'Users';
  constructor(private angularFireMessaging: AngularFireMessaging, private http: HttpClientService) {
  }

  requestPermission() {
    this.angularFireMessaging.requestPermission.subscribe((token) => {
      console.log('requestPermission ', token);
    },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }

    );
    this.angularFireMessaging.requestToken.subscribe(
      (token) => {
        console.log(token);
        this.saveNotificationToken(token);
      },
      (err) => {
        console.error('Unable to get permission to notify.', err);
      }
    );
  }

  receiveMessage() {
    this.angularFireMessaging.messages.subscribe(
      (payload: any) => {
        console.log('new message received. ', payload);
        this.currentMessage.next(payload);
      });
  }



  saveNotificationToken(token: any) {
    let url = this.base_domain_url + "/SaveNotificationToken";
    let currentUser = localStorage.getItem('currentToken');
    if (currentUser != null) {
      let data = {
        user_id: JSON.parse(currentUser).id,
        token: token
      }
      var r = JSON.parse(currentUser);
      if (r.noti_token == "" || typeof(r.noti_token) == "undefined" ) {
        this.http.post(url, data).subscribe(res => {
          console.log(res);
          r.noti_token = token;
          console.log(r);
          localStorage.setItem('currentToken', JSON.stringify(r))
        })
      }else{
        console.log(r.noti_token);
      }

    }


  }
}
