import { Component, OnInit } from '@angular/core';
import { MenuLeftService } from './menu-left.service';


@Component({
  selector: 'app-meunu-left',
  templateUrl: './meunu-left.component.html',
  styleUrls: ['./meunu-left.component.css']
})
export class MeunuLeftComponent implements OnInit {

  dichVus : any = []
  constructor(
    private menuLeftService: MenuLeftService,
  ) { }

  ngOnInit(): void {
    this.menuLeftService.getAllDichVu().subscribe(result => {

      this.dichVus = result.datas;
      sessionStorage.setItem('listDichVu', JSON.stringify(result.datas));
    })
    // this.nhaCungCaps = this.menuLeftService.getAllNhaCungCap().datas;
  }


}
