
import { Injectable } from '@angular/core';
import * as APIHelper from '../../../helper/APIDomain';
import { HttpClientService } from '../../../helper/http-client.service';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuLeftService {
  base_domain_url = APIHelper.APIUrl + 'NhaCungCaps';
  constructor(
    private http: HttpClientService
  ) { }

  getAllDichVu() : any {
    let url = this.base_domain_url + "/GetAllDichVu";
    console.log(url);
    return this.http.get(url).pipe(
      map(res => res),
      catchError(this.handleError<any>("false"))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
