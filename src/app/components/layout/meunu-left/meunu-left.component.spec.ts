import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeunuLeftComponent } from './meunu-left.component';

describe('MeunuLeftComponent', () => {
  let component: MeunuLeftComponent;
  let fixture: ComponentFixture<MeunuLeftComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeunuLeftComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeunuLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
