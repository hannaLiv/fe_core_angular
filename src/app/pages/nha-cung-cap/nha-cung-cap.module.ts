import { NhaCungCapComponent } from './nha-cung-cap.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChildListNhaCungCapComponent } from './child-list-nha-cung-cap/child-list-nha-cung-cap.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule({
  declarations: [
    NhaCungCapComponent,
    ChildListNhaCungCapComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ]
})
export class NhaCungCapModule { }
