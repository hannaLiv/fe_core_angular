import { Injectable } from '@angular/core';
import { NhaCungCapComponent } from './nha-cung-cap.component';
import * as APIHelper from '../../helper/APIDomain';
import { HttpClientService } from '../../helper/http-client.service';
import { catchError, map, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NhaCungCapService {

  base_domain_url = APIHelper.APIUrl + 'NhaCungCaps';
  constructor(
    private http: HttpClientService
  ) { }

  GetNhaCungCapByDichVuId(data: any) : any {
    let url = this.base_domain_url + "/GetNhaCungCapByDichVuId";
    return this.http.post(url, data).pipe(
      map(res => res),
      catchError(this.handleError<any>("false"))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
