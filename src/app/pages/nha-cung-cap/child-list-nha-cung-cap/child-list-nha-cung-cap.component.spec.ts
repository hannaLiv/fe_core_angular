import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildListNhaCungCapComponent } from './child-list-nha-cung-cap.component';

describe('ChildListNhaCungCapComponent', () => {
  let component: ChildListNhaCungCapComponent;
  let fixture: ComponentFixture<ChildListNhaCungCapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChildListNhaCungCapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildListNhaCungCapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
