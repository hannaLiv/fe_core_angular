import { Component, OnInit, Input,  Output, EventEmitter  } from '@angular/core';




@Component({
  selector: 'app-child-list-nha-cung-cap',
  templateUrl: './child-list-nha-cung-cap.component.html',
  styleUrls: ['./child-list-nha-cung-cap.component.css']
})
export class ChildListNhaCungCapComponent implements OnInit {
  p: number = 1;
  @Input() datas : any = []; //Day la mang cac nha cung cap truyen vao de render ra bang
  @Input() total: number = 0;
  @Input() pageSize: number = 0;

  @Output() pick_a_page = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {

  }
  ngOnChanges() {
    console.log(this.datas);
    console.log(this.total);
  }
  onPageChange($event) {
    this.p = $event;
    this.pick_a_page.emit($event);
  }


}
