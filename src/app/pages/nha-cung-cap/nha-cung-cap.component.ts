import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Route,ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { NhaCungCapService } from './nha-cung-cap.service';



@Component({
  selector: 'app-nha-cung-cap',
  templateUrl: './nha-cung-cap.component.html',
  styleUrls: ['./nha-cung-cap.component.css']
})
export class NhaCungCapComponent implements OnInit {

  //Phan khai bao
  id: number = 0;
  currentDichVu: string = "";
  pageIndex: number = 1;
  pageSize:number = 2;
  filterData: any = {
    keyword : '',
    index: this.pageIndex,
    size: this.pageSize,
    dichVuId: this.id
  }
  filterForm = this.formBuilder.group({
    keyword: ''
  });
  filterResult : any = [];
  totalRecord: number = 0;
  //Phan controller
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private formBuilder: FormBuilder,
    private nhaCungCapService: NhaCungCapService
  ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

  }
  //Phan nghiep vu
  ngOnInit(): void {
    this.getId();
    this.getCurrentDichVu();
    this.nhaCungCapService.GetNhaCungCapByDichVuId(this.filterData).subscribe(
      res => {
        console.log(res);
        this.filterResult = res.datas;
        this.totalRecord = res.totalRecord;
      }
    )
  }

  getId(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.id = id;
    this.filterData.dichVuId = id;
  }

  getCurrentDichVu(): void{
    const id = Number(this.route.snapshot.paramMap.get('id'));

    var ss_list_dv = sessionStorage.getItem('listDichVu');
    if(ss_list_dv != "" || typeof(ss_list_dv) != "undefined" || ss_list_dv != null){
      let ls = JSON.parse(ss_list_dv || "");
      this.currentDichVu = ls.filter(r => r.id == id).length > 0 ? ls.filter(r => r.id == id)[0].ten : "";
    }
  }
  onSubmitFilter(): void{
    let formValue = this.filterForm.value;
    this.filterData.keyword = formValue.keyword;
    this.nhaCungCapService.GetNhaCungCapByDichVuId(this.filterData).subscribe(
      res => {
        console.log(res);
        this.filterResult = res.datas;
        this.totalRecord = res.totalRecord;
      }
    )

  }
  public pickADate(pick_a_date: number):void {
    this.pageIndex = pick_a_date;
    this.filterData.index = this.pageIndex;
    this.nhaCungCapService.GetNhaCungCapByDichVuId(this.filterData).subscribe(
      res => {
        console.log(res);
        this.filterResult = res.datas;
        this.totalRecord = res.totalRecord;
      }
    )
  }



}
