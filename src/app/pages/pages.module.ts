import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountModule } from './account/account.module';
import { HomeModule } from './home/home.module';
import { NhaCungCapComponent } from './nha-cung-cap/nha-cung-cap.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NhaCungCapModule } from './nha-cung-cap/nha-cung-cap.module';




@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    AccountModule,
    HomeModule,
    FormsModule,
    ReactiveFormsModule,
    NhaCungCapModule
  ]

})
export class PagesModule { }
