export class loginAsyncViewModel {
  userName: string;
  passWord: string;

  constructor(loginModel:any){
    this.userName = loginModel.userName;
    this.passWord = loginModel.passWord;
  }

}
export class authenticateResponse{
  id: number;
  firstName: string;
  lastName: string;
  userName: string;
  token: string;
  constructor(authenResponse:any){
    this.id = authenResponse.id;
    this.firstName = authenResponse.firstName;
    this.lastName = authenResponse.lastName;
    this.userName = authenResponse.userName;
    this.token = authenResponse.token;
  }
}
