import { Component, Inject, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import {LoginService} from '../login/login.service';
import {Router} from "@angular/router"


@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  //Khai bao cac bien so
  loginForm = this.formBuilder.group({
    userName: '',
    passWord: ''
  });
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) { }


  ngOnDestroy(): void {
    this.renderer.removeClass(this.document.body, 'login-container');
  }

  ngOnInit(): void {
    this.renderer.addClass(this.document.body, 'login-container');
  }
  onSubmit(): void{
    let data = this.loginForm.value;
    this.loginService.loginRequest.userName = data.userName;
    this.loginService.loginRequest.passWord = data.passWord;
    this.loginService.login().subscribe(result => {
      if(result.token.length > 0){
        result.noti_token = "";
        localStorage.setItem('currentToken',JSON.stringify(result));
        this.router.navigate(['/home']);
      }



    });
  }

}
