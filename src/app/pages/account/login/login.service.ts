import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import * as APIHelper from '../../../helper/APIDomain';
import * as loginViewModel from './viewModel';



@Injectable({
  providedIn: 'root'
})


//Service class
export class LoginService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  base_domain_url = APIHelper.APIUrl + 'Users';
  constructor(private http: HttpClient) {
  }

  loginRequest = {
    userName : '',
    passWord: ''
  }

  login(): Observable<any>{
    let url = this.base_domain_url + '/LoginAsync';
    // let params = new loginViewModel.loginAsyncViewModel(this.loginRequest);
    // console.log(params);
    return this.http.post<any>(url,this.loginRequest, this.httpOptions).pipe(
      map(res => res),
      catchError(this.handleError<any>("false")));
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
