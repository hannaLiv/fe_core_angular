import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {JwtHelperService} from '@auth0/angular-jwt';
import {PushNotificationsService} from './components/pushnotifications/push-notifications.service'



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  constructor(
    private router: Router,
    private jwtHelper: JwtHelperService,
    private messagingService: PushNotificationsService

  ) { }
  title = 'angularCore';
  currentUser = localStorage.getItem('currentToken');
  isAuthen = false;
  message:any;

  ngOnInit(): void {
    let token = "";
    let userId = "";
    let userName = "";
    if(this.currentUser != null){
      let res = JSON.parse(this.currentUser);
      if(res != null){
        token = res.token;
        userId = res.id;
        userName = res.username;
      }
    }
    if(token != null && token.length > 0){
      if(!this.jwtHelper.isTokenExpired(token))
        this.isAuthen = true;
    }
    if(this.isAuthen){
      this.isAuthen = true;
      // this.router.navigate(['/home']);
    }
    else{
      this.router.navigate(['/login']);
    }
    this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage
  }



}



